# Xml Macro

A convenience macro for use with `quick-xml`, to save one from
its verbosity!

Example usage:

```rust
use xml_macro::xml;
use std::io::Cursor;
use quick_xml::Writer;

// Create the event
let event = xml!(<person name="Josh"
                         occupation={
                             let arr = ["a", "b", "c"];
                             arr[2]
                         }>);

// Write said event
let mut writer = Writer::new(Cursor::new(Vec::new()));
writer.write_event(event).unwrap();

// Check it matches our expectations
assert_eq!(
    r#"<person name="Josh" occupation="c">"#,
    String::from_utf8(writer.into_inner().into_inner()).unwrap()
)
```

Alternatively the `xmls!` macro can be used to generate an array of xml
events.

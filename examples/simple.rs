use std::io::Cursor;

use quick_xml::Writer;
use xml_macro::xml;

fn main() {
    let event = xml!(<person name = "Josh" occupation = {["a", "b", "c"][2]}>);
    let mut writer = Writer::new(Cursor::new(Vec::new()));
    writer.write_event(event).unwrap();
    println!(
        "{}",
        String::from_utf8(writer.into_inner().into_inner()).unwrap()
    )
}

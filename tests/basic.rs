use quick_xml::Writer;
use std::io::Cursor;
use xml_macro::xml;

#[test]
fn test() {
    // Create the event
    let v = "hello";
    let s = "hello".to_string();
    let event = xml!(<person name="Josh"
                             {v}
                             greeting = {s}
                             occupation={
                                 let arr = ["a", "b", "c"];
                                 arr[2]
                             }>);

    // Write said event
    let mut writer = Writer::new(Cursor::new(Vec::new()));
    writer.write_event(event).unwrap();

    // Check it matches our expectations
    assert_eq!(
        r#"<person name="Josh" v="hello" greeting="hello" occupation="c">"#,
        String::from_utf8(writer.into_inner().into_inner()).unwrap()
    )
}
